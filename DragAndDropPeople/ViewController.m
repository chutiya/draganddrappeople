//
//  ViewController.m
//  DragAndDropPeople
//
//  Created by Miniplayground on 5/15/56 BE.
//  Copyright (c) 2556 Fireoneone. All rights reserved.
//

#import "ViewController.h"
#import "RightViewController.h"
#import "FireEffect.h"
#import "SEDraggable.h"
#import "SEDraggableLocation.h"

#define OBJECT_WIDTH 60.0f
#define OBJECT_HEIGHT 60.0f
#define MARGIN_VERTICAL 10.0f
#define MARGIN_HORIZONTAL 10.0f
#define DRAGGABLE_LOCATION_WIDTH  ((OBJECT_WIDTH  * 1) + (MARGIN_HORIZONTAL * 5))
#define DRAGGABLE_LOCATION_HEIGHT ((OBJECT_HEIGHT * 6) + (MARGIN_VERTICAL   * 5))

@interface ViewController ()

{
    __weak IBOutlet UIView *_teamMemberView;
    __weak IBOutlet UIScrollView *_overlayScrollView;
    
    //RightViewController *_rightVC;
    
    __weak IBOutlet UIView *_rightVC;
    NSMutableArray *_developerImageArray;
    NSMutableArray *_designerImageArray;
    
    BOOL isRightViewShow;
    int developerAmount;
    int designerAmount;

}

@property (nonatomic, unsafe_unretained, readwrite) SEDraggableLocation *draggableLocationRight;
@property (nonatomic, unsafe_unretained, readwrite) SEDraggableLocation *draggableLocationLeft;

- (IBAction)editButtonDidtouch:(id)sender;

@end

@implementation ViewController
@synthesize draggableLocationRight = _draggableLocationRight;
@synthesize draggableLocationLeft = _draggableLocationLeft;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    /*
    if (_rightVC == nil) {
        _rightVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RightViewController"];
        //[_rightVC setDelegate:self];
    }
     */
    

    _developerImageArray = [NSMutableArray arrayWithObjects:@"banana", @"Bread1", @"Cake", @"capsicum", @"Casing_1", @"Gift", @"gift2", @"strawberry1", @"Syrup", @"Tomato", nil];
    developerAmount = [_developerImageArray count];
    
    isRightViewShow = NO;
    
        
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setupDraggableObjects {
    // set up the SEDraggables    
    for (int i=0; i < [_developerImageArray count]; i++) {
        NSString *png = [_developerImageArray objectAtIndex:i];
        UIImage *draggableImage = [UIImage imageNamed:png];
        UIImageView *draggableImageView = [[UIImageView alloc] initWithImage: draggableImage];
        draggableImageView.frame = CGRectMake(0, 0, OBJECT_WIDTH, OBJECT_HEIGHT);
        draggableImageView.backgroundColor = [UIColor clearColor];
        SEDraggable *draggable = [[SEDraggable alloc] initWithImageView: draggableImageView];
        
        [self configureDraggableObject: draggable];
    }
    
    [_overlayScrollView setContentSize:(CGSizeMake(230,((OBJECT_HEIGHT * [_developerImageArray count]) + (MARGIN_VERTICAL * ([_developerImageArray count]+2)))))];
}

- (void) configureDraggableObject:(SEDraggable *)draggable {
    draggable.homeLocation = self.draggableLocationRight;
    [draggable addAllowedDropLocation: self.draggableLocationRight];
    [draggable addAllowedDropLocation: self.draggableLocationLeft];
    [self.draggableLocationRight addDraggableObject:draggable animated:NO];
}

- (IBAction)editButtonDidtouch:(id)sender
{
    /*
    if (_rightVC == nil) {
        _rightVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RightViewController"];
        //[_rightVC setDelegate:self];
        
    }
    */
    
    if (isRightViewShow == NO) {
        [UIView animateWithDuration:0.3f animations:^{_teamMemberView.frame = CGRectMake(13.0f, 120.0f, 203.0f, 340.0f);}];
        
        [self.view addSubview:_overlayScrollView];
        //[_overlayScrollView addSubview:_rightVC];
        //_rightVC.frame = CGRectMake(0.0f, 0.0f, _rightVC.frame.size.width, _rightVC.frame.size.height);
        
        SEDraggableLocation *draggableLocationRight = [[SEDraggableLocation alloc] initWithFrame:CGRectMake(0.0f, 10.0f, DRAGGABLE_LOCATION_WIDTH, ((OBJECT_HEIGHT * developerAmount) + (MARGIN_VERTICAL * (developerAmount+2))))];
        SEDraggableLocation *draggableLocationLeft = [[SEDraggableLocation alloc] initWithFrame:CGRectMake(0.0f, 0.0f, _teamMemberView.frame.size.width, _teamMemberView.frame.size.height)];
        
        
        [_overlayScrollView addSubview:draggableLocationRight];
        [_teamMemberView addSubview:draggableLocationLeft];
        
        draggableLocationRight.backgroundColor = [UIColor clearColor];
        draggableLocationLeft.backgroundColor = [UIColor clearColor];
        
        [FireEffect slideInFromRight:_overlayScrollView origin_x:230.0f origin_y:0.0f duration:0.3f];
        isRightViewShow = YES;
  
        
        [self configureDraggableLocation: draggableLocationRight];
        [self configureDraggableLocation: draggableLocationLeft];
        
        self.draggableLocationRight = draggableLocationRight;
        self.draggableLocationLeft = draggableLocationLeft;
        
        
    } else {
        [UIView animateWithDuration:0.3f animations:^{_teamMemberView.frame = CGRectMake(13.0f, 120.0f, 293.0f, 340.0f);}];
        
        
        [FireEffect slideInFromLeft:_overlayScrollView origin_x:230.0f origin_y:0.0f duration:0.3f];
        [_overlayScrollView removeFromSuperview];
        _rightVC = nil;
        isRightViewShow = NO;
    
    }
    
    //[self setupDraggableLocations];
    [self setupDraggableObjects];

}

- (void) setupDraggableLocations {
    // set up the SEDraggableLocations
    //CGFloat locationXCoord = CGRectGetMidX(self.view.frame) - (DRAGGABLE_LOCATION_WIDTH / 2.0f);
    SEDraggableLocation *draggableLocationRight = [[SEDraggableLocation alloc] initWithFrame:_overlayScrollView.frame];
    SEDraggableLocation *draggableLocationLeft = [[SEDraggableLocation alloc] initWithFrame:_teamMemberView.frame];
    
    // you always want your SEDraggableLocations to be transparent -- otherwise, SEDraggable
    // objects will sometimes seem to hide behind certain locations while being dragged
    draggableLocationRight.backgroundColor = [UIColor clearColor];
    draggableLocationLeft.backgroundColor = [UIColor clearColor];
    
    // ... however, we can put clear SEDraggableLocations in front of UIViews
    // that have background images or colors to circumvent this obstacle
    UIView *rightWrapper    = [[UIView alloc] initWithFrame: draggableLocationRight.frame];
    UIView *leftWrapper = [[UIView alloc] initWithFrame: draggableLocationLeft.frame];
    rightWrapper.backgroundColor    = [UIColor redColor];
    leftWrapper.backgroundColor = [UIColor blueColor];
    [self.view addSubview: rightWrapper];
    [self.view addSubview: leftWrapper];
    [self.view addSubview: draggableLocationRight];
    [self.view addSubview: draggableLocationLeft];
    
    
    //[self configureDraggableLocation: draggableLocationTop];
    //[self configureDraggableLocation: draggableLocationBottom];
    
    //self.draggableLocationTop = draggableLocationTop;
    //self.draggableLocationBottom = draggableLocationBottom;
}

- (void) configureDraggableLocation:(SEDraggableLocation *)draggableLocation {
    // set the width and height of the objects to be contained in this SEDraggableLocation (for spacing/arrangement purposes)
    draggableLocation.objectWidth = OBJECT_WIDTH;
    draggableLocation.objectHeight = OBJECT_HEIGHT;
    
    // set the bounding margins for this location
    draggableLocation.marginLeft = MARGIN_HORIZONTAL;
    draggableLocation.marginRight = MARGIN_HORIZONTAL;
    draggableLocation.marginTop = MARGIN_VERTICAL;
    draggableLocation.marginBottom = MARGIN_VERTICAL;
    
    // set the margins that should be preserved between auto-arranged objects in this location
    draggableLocation.marginBetweenX = MARGIN_HORIZONTAL;
    draggableLocation.marginBetweenY = MARGIN_VERTICAL;
    
    // set up highlight-on-drag-over behavior
    draggableLocation.highlightColor = [UIColor greenColor].CGColor;
    draggableLocation.highlightOpacity = 0.4f;
    draggableLocation.shouldHighlightOnDragOver = YES;
    
    // you may want to toggle this on/off when certain events occur in your app
    draggableLocation.shouldAcceptDroppedObjects = YES;
    
    // set up auto-arranging behavior
    draggableLocation.shouldKeepObjectsArranged = YES;
    draggableLocation.fillHorizontallyFirst = YES; // NO makes it fill rows first
    draggableLocation.allowRows = YES;
    draggableLocation.allowColumns = YES;
    draggableLocation.shouldAnimateObjectAdjustments = YES; // if this is set to NO, objects will simply appear instantaneously at their new positions
    draggableLocation.animationDuration = 0.5f;
    draggableLocation.animationDelay = 0.0f;
    draggableLocation.animationOptions = UIViewAnimationOptionLayoutSubviews ; // UIViewAnimationOptionBeginFromCurrentState;
    
    draggableLocation.shouldAcceptObjectsSnappingBack = YES;
}

@end
