//
//  RightViewController.m
//  DragAndDropPeople
//
//  Created by Miniplayground on 5/16/56 BE.
//  Copyright (c) 2556 Fireoneone. All rights reserved.
//

#import "RightViewController.h"
#import "SEDraggable.h"
#import "SEDraggableLocation.h"

@interface RightViewController ()
{
    
}


@property (nonatomic, unsafe_unretained, readwrite) SEDraggableLocation *draggableLocationTop;
@property (nonatomic, unsafe_unretained, readwrite) SEDraggableLocation *draggableLocationBottom;

@end

@implementation RightViewController

@synthesize draggableLocationTop = _draggableLocationTop;
@synthesize draggableLocationBottom = _draggableLocationBottom;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
